package com.humayoun.a20220514_muhammadhumayoun_nycschools

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.humayoun.a20220514_muhammadhumayoun_nycschools.api.SchoolsService
import com.humayoun.a20220514_muhammadhumayoun_nycschools.repository.SchoolsRepository
import com.humayoun.a20220514_muhammadhumayoun_nycschools.ui.SchoolDetails.SchoolDetailsViewModel
import com.humayoun.a20220514_muhammadhumayoun_nycschools.ui.main.MainViewModel
import java.lang.IllegalArgumentException


/**
 * Object(class) that handles object creation.
 * objects can be passed as parameters in the constructors and then replaced for
 * testing, where needed.
 **/
object Injection {

    fun provideSchoolsRepository(): SchoolsRepository {
        return SchoolsRepository(SchoolsService.create())
    }

    fun provideViewModelFactory(): ViewModelProvider.Factory {
        return object: ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                if(modelClass.isAssignableFrom(MainViewModel::class.java)){
                    return MainViewModel(provideSchoolsRepository()) as T
                }

                if(modelClass.isAssignableFrom(SchoolDetailsViewModel::class.java)){
                    return SchoolDetailsViewModel(provideSchoolsRepository()) as T
                }

                throw IllegalArgumentException("unknown view model class")
            }
        }
    }

}