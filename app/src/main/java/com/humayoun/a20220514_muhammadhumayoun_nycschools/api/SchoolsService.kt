package com.humayoun.a20220514_muhammadhumayoun_nycschools.api

import com.humayoun.a20220514_muhammadhumayoun_nycschools.models.Sat
import com.humayoun.a20220514_muhammadhumayoun_nycschools.models.School
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsService {

    @GET("resource/s3k6-pzi2")
    suspend fun getAllSchools(): List<School>

    @GET("resource/f9bf-2cp4")
    suspend fun getSatInfo(@Query("dbn") id: String): List<Sat>

    companion object {
        private const val BASE_URL = "https://data.cityofnewyork.us"

        fun create(): SchoolsService {
            val httpClient = OkHttpClient.Builder().build()

            val retrofitClient = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(httpClient)
                .build()

            return retrofitClient.create(SchoolsService::class.java)
        }
    }
}