package com.humayoun.a20220514_muhammadhumayoun_nycschools.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData

import com.humayoun.a20220514_muhammadhumayoun_nycschools.api.SchoolsService
import com.humayoun.a20220514_muhammadhumayoun_nycschools.models.Sat
import com.humayoun.a20220514_muhammadhumayoun_nycschools.models.School
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

/** this repository class is responsible for getting the data, if we implement
 * offline storage then this class will fetch data from sql/Room database as well
 * for now it's only dependency is schoolService, which is passed in constructor for dependency injection and easy testing*
 * */
class SchoolsRepository(private val schoolsService: SchoolsService) {
    private val TAG = "SchoolsRepository"
    val schoolsListData = MutableLiveData<List<School>>()
    val satScore = MutableLiveData<Sat>()
    val isLoading = MutableLiveData<Boolean>(false)
    val errorMessage = MutableLiveData<String?>(null)

    /**
     *  If Given more time: To speed up initial load time This function could be paginated
     *  to only fetch limited amount of schools and when user scrolls
     *  down we would fetch more, something like fun getSchoolsData(page:Int, limit: Int)
     */
    // get all schools data
    fun getSchoolsData() {
        isLoading.value = true
        errorMessage.value = null

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val schools = schoolsService.getAllSchools()
                schoolsListData.postValue(schools)
            } catch (e: Exception) {
                errorMessage.postValue(e.message)
            } finally {
                isLoading.postValue(false)
            }
        }
    }

    // Get sat scores for a school id
    fun getSatScores(schoolId: String) {
        isLoading.value = true
        errorMessage.value = null

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val satScores = schoolsService.getSatInfo(schoolId)
                if(!satScores.isEmpty()) {
                    satScore.postValue(satScores[0])
                }
            } catch (e: Exception) {
                errorMessage.postValue(e.message)
                Log.i(TAG,"Exception ${e}")
            } finally {
                isLoading.postValue(false)
            }
        }
    }

}