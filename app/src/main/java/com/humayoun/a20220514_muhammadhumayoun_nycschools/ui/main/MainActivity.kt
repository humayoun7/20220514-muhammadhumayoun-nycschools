package com.humayoun.a20220514_muhammadhumayoun_nycschools.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.humayoun.a20220514_muhammadhumayoun_nycschools.Injection
import com.humayoun.a20220514_muhammadhumayoun_nycschools.R
import com.humayoun.a20220514_muhammadhumayoun_nycschools.constants.Constants
import com.humayoun.a20220514_muhammadhumayoun_nycschools.databinding.ActivityMainBinding
import com.humayoun.a20220514_muhammadhumayoun_nycschools.models.School
import com.humayoun.a20220514_muhammadhumayoun_nycschools.ui.SchoolDetails.SchoolDetailsActivity


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var schoolListAdapter: SchoolsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // get viewModel from the view model factory
        viewModel = ViewModelProvider(this, Injection.provideViewModelFactory()).get(MainViewModel::class.java)
        viewModel.getSchoolsData()

        setObservers()
    }

    fun setObservers() {
        // for recycler view
        viewModel.schoolsListData.observe(this, Observer { schools ->
            // create school list adpater
            schoolListAdapter = SchoolsAdapter(schools, object: SchoolsAdapter.ListItemClickListener {
                override fun onListItemClick(school: School) {
                    val intent = Intent(this@MainActivity, SchoolDetailsActivity::class.java)
                    intent.putExtra(Constants.SCHOOL_DATA_KEY, school)
                    startActivity(intent)
                }
            })

            // set data for recycler view
            binding.schoolsRecyclerView.adapter = schoolListAdapter
            binding.schoolsRecyclerView.layoutManager = LinearLayoutManager(this)
        })

        // for loading indicator
        viewModel.isLoading.observe(this) {
            binding.progressBar.visibility = if (it) View.VISIBLE else  View.GONE
        }

        // for error messages
        viewModel.errorMessage.observe(this) { error ->
            error?.let {
                Toast.makeText(this, resources.getString(R.string.generic_error_message), Toast.LENGTH_LONG).show()
            }
        }
    }
}