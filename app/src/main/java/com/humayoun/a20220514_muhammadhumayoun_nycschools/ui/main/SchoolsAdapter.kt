package com.humayoun.a20220514_muhammadhumayoun_nycschools.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.humayoun.a20220514_muhammadhumayoun_nycschools.R
import com.humayoun.a20220514_muhammadhumayoun_nycschools.databinding.SchoolListItemBinding
import com.humayoun.a20220514_muhammadhumayoun_nycschools.models.School


/**
 * Adapter to bind recycler view with school data,
 * */

class SchoolsAdapter(private val schoolList: List<School>,
                     private val listItemClickListener: ListItemClickListener):
                            RecyclerView.Adapter<SchoolsAdapter.ViewHolder>() {


    inner  class ViewHolder(val itemView: View): RecyclerView.ViewHolder(itemView) {
        val binding = SchoolListItemBinding.bind(itemView)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflator = LayoutInflater.from(parent.context)
        val view = inflator.inflate(R.layout.school_list_item, parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val school = schoolList[position]
        holder.binding.schoolNametextView.text = school.name
        holder.binding.schoolCitytextView.text = school.city
        holder.binding.root.setOnClickListener{
            listItemClickListener.onListItemClick(school)
        }
    }

    override fun getItemCount() = schoolList.size

    /**
     * interface to handle callbacks for click action on the recycler view
     * */
    interface ListItemClickListener {
        fun onListItemClick(school: School)
    }
}