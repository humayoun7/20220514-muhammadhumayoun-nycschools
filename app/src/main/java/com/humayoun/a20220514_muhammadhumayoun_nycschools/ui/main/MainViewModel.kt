package com.humayoun.a20220514_muhammadhumayoun_nycschools.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.humayoun.a20220514_muhammadhumayoun_nycschools.api.SchoolsService
import com.humayoun.a20220514_muhammadhumayoun_nycschools.models.School
import com.humayoun.a20220514_muhammadhumayoun_nycschools.repository.SchoolsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

/**
 * MainViewModel depends on schoolsRepository to provide data,
 * it dependency is passed in constructor for decoupling (easy dependency injection and testing)
 * It is responsible to  control application/business logic, for a small project like this we could have
 * called schoolsserice directly from viewmodel scope but by decoupling data layer
 * gives us good grounds to unit test
 */
class MainViewModel(private val schoolsRepository: SchoolsRepository): ViewModel() {

    val schoolsListData: LiveData<List<School>>
        get() = schoolsRepository.schoolsListData

    val isLoading: LiveData<Boolean>
        get() = schoolsRepository.isLoading

    val errorMessage: LiveData<String?>
        get() = schoolsRepository.errorMessage

    fun getSchoolsData() {
        schoolsRepository.getSchoolsData()
    }

}