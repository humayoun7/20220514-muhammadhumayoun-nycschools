package com.humayoun.a20220514_muhammadhumayoun_nycschools.ui.SchoolDetails

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.humayoun.a20220514_muhammadhumayoun_nycschools.Injection
import com.humayoun.a20220514_muhammadhumayoun_nycschools.R
import com.humayoun.a20220514_muhammadhumayoun_nycschools.constants.Constants
import com.humayoun.a20220514_muhammadhumayoun_nycschools.databinding.ActivitySchoolDetailsBinding
import com.humayoun.a20220514_muhammadhumayoun_nycschools.models.School

class SchoolDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySchoolDetailsBinding
    private lateinit var viewModel: SchoolDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // receive school data from the main activity
        val school = intent.getParcelableExtra<School>(Constants.SCHOOL_DATA_KEY)

        // showing back button in the action bar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // get viewModel from the view model factory
        viewModel = ViewModelProvider(this, Injection.provideViewModelFactory()).get(SchoolDetailsViewModel::class.java)

        // for setting school data
        school?.let{
            viewModel.getSatScores(it.id)

            binding.schoolNameHeadingTextView.text = school.name
            binding.overviewTextView.text = school.overview
        }

        setObservers()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun setObservers() {
        // for setting scores
        viewModel.satScore.observe(this) { sat ->
            binding.mathScoreTextView.text = sat.mathAvgScore.ifBlank { "n/a" }
            binding.readingScoreTextView.text = sat.readingAvgScore.ifBlank { "n/a" }
            binding.writingScoreTextView.text = sat.writingAvgScore.ifBlank { "n/a" }
        }

        // for loading indicator
        viewModel.isLoading.observe(this) {
            binding.schoolDetailsProgressBar.visibility = if (it) View.VISIBLE else  View.GONE
        }

        // for error messages
        viewModel.errorMessage.observe(this) {
            it?.let {
                Toast.makeText(this, resources.getString(R.string.generic_error_message), Toast.LENGTH_LONG).show()
            }
        }
    }
}