package com.humayoun.a20220514_muhammadhumayoun_nycschools.ui.SchoolDetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.humayoun.a20220514_muhammadhumayoun_nycschools.models.Sat
import com.humayoun.a20220514_muhammadhumayoun_nycschools.repository.SchoolsRepository


/**
 * SchoolDetailsViewModel depends on schoolsRepository to provide data,
 * it's dependency is passed in constructor for decoupling (easy dependency injection and testing)
 * It is responsible to  control application/business logic, for a small project like this we could have
 * called schoolsserice directly from viewmodel scope but by decoupling data layer
 * gives us good grounds to unit test
 */
class SchoolDetailsViewModel(private val schoolsRepository: SchoolsRepository): ViewModel() {

    val isLoading: LiveData<Boolean>
        get() = schoolsRepository.isLoading

    val satScore: LiveData<Sat>
        get() = schoolsRepository.satScore

    val errorMessage: LiveData<String?>
        get() = schoolsRepository.errorMessage


    fun getSatScores(schoolId: String) {
        schoolsRepository.getSatScores(schoolId)
    }

}