package com.humayoun.a20220514_muhammadhumayoun_nycschools.models

import com.squareup.moshi.Json

data class Sat (
    @field:Json(name = "dbn") val id: String,
    @field:Json(name = "school_name") val schoolName: String,
    @field:Json(name = "num_of_sat_test_takers") val noOfTestTakers: String,
    @field:Json(name = "sat_critical_reading_avg_score") val readingAvgScore: String,
    @field:Json(name = "sat_math_avg_score") val mathAvgScore: String,
    @field:Json(name = "sat_writing_avg_score") val writingAvgScore: String
        )