package com.humayoun.a20220514_muhammadhumayoun_nycschools.models

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

/**
 * School implements parcelize so that we can easily pass selected school data
 * to school details activity
 * */

@Parcelize
data class School (
    @field:Json(name="dbn") val id: String,
    @field:Json(name="school_name") val name: String,
    @field:Json(name="overview_paragraph") val overview: String,
    @field:Json(name="city") val city: String,
    @field:Json(name="zip") val zip: String,
    ) : Parcelable